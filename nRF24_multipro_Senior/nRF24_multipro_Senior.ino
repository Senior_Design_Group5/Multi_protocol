/*********************************************************************************


  ##########################################
  #####   MultiProtocol nRF24L01 Tx   ######
  ##########################################
  #        by goebish on rcgroups          #
  #                                        #
  #   Parts of this project are derived    #
  #     from existing work, thanks to:     #
  #                                        #
  #   - PhracturedBlue for DeviationTX     #
  #   - victzh for XN297 emulation layer   #
  #   - Hasi for Arduino PPM decoder       #
  #   - hexfet, midelic, closedsink ...    #
  ##########################################


  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License.
  If not, see <http://www.gnu.org/licenses/>.
*/
#include <SoftwareSerial.h>
#include <util/atomic.h>
#include <EEPROM.h>
#include "iface_nrf24l01.h"
#include <string.h>

#include <WiFiEsp.h>
#include <WiFiEspUdp.h>

#include <TinyGPS++.h>

// ############ Wiring ################
#define PPM_pin 2 // PPM in
//SPI Comm.pins with nRF24L01
#define MOSI_pin  3  // MOSI - D3
#define SCK_pin   5  // SCK  - D5
#define CE_pin    4  // CE   - D4
#define MISO_pin  A1 // MISO - A1
#define CS_pin    A2 // CS   - A2

#define ledPin 13 // LED  - D13

// SPI outputs
#define MOSI_on PORTD |= _BV(3)  // PD3
#define MOSI_off PORTD &= ~_BV(3)// PD3
#define SCK_on PORTD |= _BV(5)   // PD5
#define SCK_off PORTD &= ~_BV(5) // PD5
#define CE_on PORTD |= _BV(4)    // PD4
#define CE_off PORTD &= ~_BV(4)  // PD4
#define CS_on PORTC |= _BV(2)    // PC2
#define CS_off PORTC &= ~_BV(2)  // PC2
// SPI input
#define  MISO_on (PINC & _BV(1)) // PC1

#define RF_POWER TX_POWER_80mW

// PPM stream settings
#define CHANNELS 12 // number of channels in ppm stream, 12 ideally
enum chan_order {
  THROTTLE, //up down
  AILERON, //left right
  ELEVATOR, //forward back
  RUDDER, //rotate
  AUX1,  // (CH5)  led light, or 3 pos. rate on CX-10, H7, or inverted flight on H101
  AUX2,  // (CH6)  flip control
  AUX3,  // (CH7)  still camera (snapshot)
  AUX4,  // (CH8)  video camera
  AUX5,  // (CH9)  headless
  AUX6,  // (CH10) calibrate Y (V2x2), pitch trim (H7), RTH (Bayang, H20), 360deg flip mode (H8-3D, H22)
  AUX7,  // (CH11) calibrate X (V2x2), roll trim (H7)
  AUX8,  // (CH12) Reset / Rebind
};

#define PPM_MIN 1000
#define PPM_SAFE_THROTTLE 1050
#define PPM_MID 1500
#define PPM_MAX 2000
#define PPM_MIN_COMMAND 1300
#define PPM_MAX_COMMAND 1700
#define GET_FLAG(ch, mask) (ppm[ch] > PPM_MAX_COMMAND ? mask : 0)

// supported protocols
enum {
  PROTO_V2X2 = 0,     // WLToys V2x2, JXD JD38x, JD39x, JJRC H6C, Yizhan Tarantula X6 ...
  PROTO_CG023,        // EAchine CG023, CG032, 3D X4
  PROTO_CX10_BLUE,    // Cheerson CX-10 blue board, newer red board, CX-10A, CX-10C, Floureon FX-10, CX-Stars (todo: add DM007 variant)
  PROTO_CX10_GREEN,   // Cheerson CX-10 green board
  PROTO_H7,           // EAchine H7, MoonTop M99xx
  PROTO_BAYANG,       // EAchine H8(C) mini, H10, BayangToys X6, X7, X9, JJRC JJ850, Floureon H101
  PROTO_SYMAX5C1,     // Syma X5C-1 (not older X5C), X11, X11C, X12
  PROTO_YD829,        // YD-829, YD-829C, YD-822 ...
  PROTO_H8_3D,        // EAchine H8 mini 3D, JJRC H20, H22
  PROTO_END
};

// EEPROM locations
enum {
  ee_PROTOCOL_ID = 0,
  ee_TXID0,
  ee_TXID1,
  ee_TXID2,
  ee_TXID3
};

uint8_t transmitterID[4];
uint8_t current_protocol;
static volatile bool ppm_ok = false;
uint8_t packet[32];
static bool reset = true;
volatile uint16_t Servo_data[12];
static uint16_t ppm[12] = {PPM_MIN, PPM_MID, PPM_MID, PPM_MID, PPM_MID, PPM_MID,
                           PPM_MID, PPM_MID, PPM_MID, PPM_MID, PPM_MID, PPM_MID,
                          };

//Atonomous Flight parameters
long lastmeasurement_time = 0;
long lastmeasurement_time2 = 0;
bool startFlight = false; // True if Start message was received
bool stopFlight = false;  // True if Stop message was received
bool startHover = false;  // True if Hover message was received

//Sonic Sensor parameters
const int trigPin = 12;
const int echoPin = 11;

//Wifi module parameters
char ssid[] = "aptest";            // your network SSID (name)
char pass[] = "ardtest12";        // your network password
int status = WL_IDLE_STATUS;     // the Wifi radio's status
unsigned int localPort = 10002;  // local port to listen on
char packetBuffer[255];          // buffer to hold incoming packet
char ReplyBuffer[] = "RECEIVED";      // a string to send back
WiFiEspUDP Udp;

//GPS module
/*static const int RXPin = 4, TXPin = 3;
static const uint32_t GPSBaud = 9600;
//static const int N = 0, E = 90, S = 180, W = 270;*/

// ESP Serial
SoftwareSerial Serial1(9,10); // RX, TX

//GPS Serial
//SoftwareSerial Serial2(RXPin, TXPin); // RX, TX

void setup()
{
  //sonic sensor pins
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  //Start nrf24l01
  randomSeed((analogRead(A4) & 0x1F) | (analogRead(A5) << 5));
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW); //start LED off
  pinMode(PPM_pin, INPUT);
  pinMode(MOSI_pin, OUTPUT);
  pinMode(SCK_pin, OUTPUT);
  pinMode(CS_pin, OUTPUT);
  pinMode(CE_pin, OUTPUT);
  pinMode(MISO_pin, INPUT);

  // PPM ISR setup
  TCCR1A = 0;  //reset timer1
  TCCR1B = 0;
  TCCR1B |= (1 << CS11);  //set timer1 to increment every 1 us @ 8MHz, 0.5 us @16MHz
  set_txid(false);

  // Serial port input/output setup
  Serial.begin(115200);
  Serial.println("Starting");

  // Initialize serial for ESP module
  Serial1.begin(115200);
  WiFi.init(&Serial1);
  Serial1.println("AT+UART_DEF=9600,8,1,0,0");//software serial has trouble at 115200 so we must set the esp baud to 9600
  Serial1.begin(9600);
  WiFi.init(&Serial1);

  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    while (true);
  }

  // Attempt to connect to WiFi network
  while ( status != WL_CONNECTED) {
    //Serial.print("Attempting to connect to WPA SSID: ");
    //Serial.println(ssid);
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);
  }

  // Begin GPS init
  //Serial2.begin(GPSBaud);
  
  printWifiStatus();
  Udp.begin(localPort);
  Serial.println("Connected to wifi");

}

void loop()
{
  uint32_t timeout;
  if (reset || ppm[AUX8] > PPM_MAX_COMMAND) {
    resetAndRebind();
  }

  //IMPORTANT!!!
  //This not only sets the timeout but also transmits the data over the nrf24l01
  timeout = process_SymaX();

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    checkPacket();
  }

  // wait before sending next packet
    while(micros() < timeout);

//If start message was received begin autonomous flight
  if (startFlight == true) {
    flight();
    Serial.println("END FLIGHT");
    startFlight = false;
  }
}


void set_txid(bool renew)
{
  uint8_t i;
  for (i = 0; i < 4; i++)
    transmitterID[i] = EEPROM.read(ee_TXID0 + i);
  if (renew || (transmitterID[0] == 0xFF && transmitterID[1] == 0x0FF)) {
    for (i = 0; i < 4; i++) {
      transmitterID[i] = random() & 0xFF;
      EEPROM.update(ee_TXID0 + i, transmitterID[i]);
    }
  }
}

void selectProtocol()
{
  // wait for multiple complete ppm frames
  ppm_ok = false;
  set_txid(true);                      // Renew Transmitter ID

  // protocol selection
  current_protocol = PROTO_SYMAX5C1; // Syma X5C-1, X11, X11C, X12
  // update eeprom
  EEPROM.update(ee_PROTOCOL_ID, current_protocol);
}

void init_protocol()
{
  Symax_init();
  SymaX_bind();
}

void printWifiStatus() {
  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  long rssi = WiFi.RSSI();
}

void resetAndRebind() {
  reset = false;
    Serial.println("selecting protocol");
    selectProtocol();
    Serial.println("selected protocol.");
    NRF24L01_Reset();
    Serial.println("nrf24l01 reset.");
    NRF24L01_Initialize();
    Serial.println("nrf24l01 init.");
    init_protocol();
    Serial.println("init protocol complete.");
}

void checkPacket() {
  Serial.print("Received packet of size ");
  IPAddress remoteIp = Udp.remoteIP();
  int len = Udp.read(packetBuffer, 255); // read the packet into packetBufffer
  
  if (len > 0) {
    packetBuffer[len] = 0;
  }
  
  Serial.println("Contents:");
  String pak = "";
  pak = String(pak + packetBuffer);
  Serial.println(packetBuffer);
  getAction(pak);
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());// send a reply, to the IP address and port that sent us the packet we received
  Udp.write(ReplyBuffer);
  Udp.endPacket();
}

void getAction(String pak) {
  if (pak.equals("Hover")) {
    Serial.println("OVERRIDE");
    Serial.println("BEGIN DANCE");
    startHover = true;
    lastmeasurement_time = millis();
  } else if ("Stop") {
    Serial.println("STOP FLIGHT");
    Serial.println("LAND");
    stopFlight = true;
    lastmeasurement_time = millis();
  } else if ("Begin") {
    Serial.println("BEGIN MISSION");
    startFlight = true;
    lastmeasurement_time = millis();
  }
}

